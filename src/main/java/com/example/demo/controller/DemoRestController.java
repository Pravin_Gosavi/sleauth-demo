package com.example.demo.controller;


import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.cloud.sleuth.sampler.AlwaysSampler;

//import java.util.logging.Level;
//import java.util.logging.Logger;


/**
 * Created by pravingosavi on 23/11/18.
 */

@RestController
public class DemoRestController {

    private static final Logger LOG = Logger.getLogger(DemoRestController.class.getName());

    @Autowired
    private RestTemplate restTemplate;

    @Bean
    public RestTemplate getRestTemplate(){
        return new RestTemplate();
    }

    @Bean
    public AlwaysSampler alwaysSampler() {
        return new AlwaysSampler();
    }

    @RequestMapping(value = "/")
    public String getIndexPage(){
        LOG.log(Level.INFO,"This is Index Page");
        return "Hello World....";
    }


    @RequestMapping(value = "/home")
    public String getHomePage(){
        LOG.log(Level.INFO,"This is Home Page");
        return "Home Page Success..";
    }



}
